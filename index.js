// Array Destructuring

// const listPenduduk = ["Jono", "Joko", "Jeni", "Jessie"];

// const [penduduk1, penduduk2, penduduk3, penduduk4] = listPenduduk;

// console.log(penduduk1, penduduk3);

const penduduk = {
    nama: "Jono",
    pekerjaan: "Pelaut",
    nomorRumah: 24,
    pendapatan: 12500000,
    anak: {
        namaAnak: "Joni",
        pekerjaanAnak: "Pelajar"
    }
}

// Alias and Default Value
const {
    nama: namaPenduduk,
    istri = "Jessica",
    pendapatan,
    anak: { namaAnak },
    ...dataLainnya
} = penduduk;

console.log(namaPenduduk, istri, pendapatan, namaAnak);

// Function Parameter Destructuring
// function printInfoPenduduk ({nama, pekerjaan, pendapatan}) {
//     const pesan = `Pak ${nama} bekerja sebagai ${pekerjaan}. Ia memiliki penghasilan sejumlah Rp.${pendapatan} perbulannya.`
//     console.log(pesan);
// }
// printInfoPenduduk(penduduk);

// Object Destructuring
// const {nama, pekerjaan, nomorRumah} = penduduk;
// console.log(nama, nomorRumah);